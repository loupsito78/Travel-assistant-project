package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

@Dao
public interface ToDoListDAO {
    @Query("SELECT * FROM ToDoList")
    java.util.List<ToDoList> getAll();

    @Insert
    void insertAll(ToDoList... toDoLists);

    @Insert
    void insert(ToDoList toDoList);

    @Delete
    void delete(ToDoList toDoList);

    @Query("DELETE FROM ToDoList")
    public void nukeTable();
}
