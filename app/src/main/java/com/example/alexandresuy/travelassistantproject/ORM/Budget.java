package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import java.util.Date;

@Entity(indices = {@Index("id")})
public class Budget {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "currentBudget")
    private int currentBudget;

    @ColumnInfo(name = "limitBudget")
    private int limitBudget;

    public Budget(int currentBudget, int limitBudget) {
        this.currentBudget = currentBudget;
        this.limitBudget = limitBudget;
    }

    public int getId() {
        return id;
    }

    public int getCurrentBudget() {
        return currentBudget;
    }

    public int getLimitBudget() {
        return limitBudget;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCurrentBudget(int currentBudget) {
        this.currentBudget = currentBudget;
    }

    public void setLimitBudget(int limitBudget) {
        this.limitBudget = limitBudget;
    }
}
