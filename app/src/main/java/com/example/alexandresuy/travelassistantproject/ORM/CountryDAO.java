package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;


@Dao
public interface CountryDAO {
    @Query("SELECT * FROM Country")
    List<Country> getAll();

    @Query("SELECT name FROM Country")
    List<String> getAllNameCountry();

    @Query("SELECT name FROM COUNTRY WHERE id= :paramId")
    String getNameCountryFromId(int paramId);

    @Query("SELECT id FROM COUNTRY WHERE name = :paramName")
    int getIdCountryFromName(String paramName);

    @Insert
    void insertAll(Country... countries);

    @Insert
    void insert(Country country);

    @Delete
    void delete(Country country);

    @Query("DELETE FROM Country")
    public void nukeTable();
}