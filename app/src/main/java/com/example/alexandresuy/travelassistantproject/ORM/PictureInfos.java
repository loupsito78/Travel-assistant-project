package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

@Entity(indices = {@Index("id")})
public class PictureInfos {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "idTrip")
    private int idTrip;

    @ColumnInfo(name = "titlePicture")
    private String titlePicture;

    public PictureInfos(int idTrip, String titlePicture) {
        this.idTrip = idTrip;
        this.titlePicture = titlePicture;
    }

    public int getId() {
        return id;
    }

    public int getIdTrip() {
        return idTrip;
    }

    public String getTitlePicture() {
        return titlePicture;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }

    public void setTitlePicture(String titlePicture) {
        this.titlePicture = titlePicture;
    }
}
