package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface BudgetDAO {
    @Query("SELECT * FROM Budget")
    List<Budget> getAll();

    @Query("SELECT * FROM Budget WHERE id=:id")
    Budget findBudgetFromId(int id);

    @Query("SELECT sum(amount) FROM Spending where idBudget= :idBudget")
    int getAmountBudget(int idBudget);

    @Insert
    void insertAll(Budget... budgets);

    @Insert
    long insert(Budget budget);

    @Delete
    void delete(Budget budget);

    @Query("DELETE FROM Budget")
    public void nukeTable();
}
