package com.example.alexandresuy.travelassistantproject.Converters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.alexandresuy.travelassistantproject.ORM.CheckList;
import com.example.alexandresuy.travelassistantproject.R;

import java.util.ArrayList;


public class CheckListAdapter extends ArrayAdapter<CheckList> implements View.OnClickListener{

    private ArrayList<CheckList> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView title;
    }

    public CheckListAdapter(ArrayList<CheckList> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;
    }

    @Override
    public void onClick(View v) {

    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        CheckList checkList = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_checklist, parent, false);
            viewHolder.title = (TextView) convertView.findViewById(R.id.name);
            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        lastPosition = position;
        viewHolder.title.setText(checkList.getName());
        // Return the completed view to render on screen
        return convertView;
    }
}
