package com.example.alexandresuy.travelassistantproject.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alexandresuy.travelassistantproject.Converters.BudgetAdapter;
import com.example.alexandresuy.travelassistantproject.Converters.BudgetModel;
import com.example.alexandresuy.travelassistantproject.ORM.AppDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.MyDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.Spending;
import com.example.alexandresuy.travelassistantproject.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static com.example.alexandresuy.travelassistantproject.Fragments.MainFragment.getIdTrip;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BudgetFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BudgetFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BudgetFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    //To get the view
    View view;

    //List to structure the spending in a budget for the ListView
    ArrayList<BudgetModel> budgetModels;

    //Adaptater for the budget
    private static BudgetAdapter adapter;

    //Parameter auto generate
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private int currentValueProgressBar;

    public BudgetFragment() {
        // Required empty public constructor
    }

    public int getCurrentValueProgressBar() {
        return currentValueProgressBar;
    }

    public void setCurrentValueProgressBar(int currentValueProgressBar) {
        this.currentValueProgressBar = currentValueProgressBar;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BudgetFragment.
     */
    public static BudgetFragment newInstance(String param1, String param2) {
        BudgetFragment fragment = new BudgetFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    /**
     * Create a popup for a new spending
     */
    public void popUpNewSpending(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setTitle("New Spending");
        builder.setMessage("Add a new spending");
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // EditText in the popup
        final EditText titleSpendind = new EditText(getContext());
        final EditText AmountSpendind = new EditText(getContext());

        //Set type
        titleSpendind.setInputType(InputType.TYPE_CLASS_TEXT);
        AmountSpendind.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        //Set placeholder
        titleSpendind.setHint("Title");
        AmountSpendind.setHint("Amount");

        //Set a layout for the popup
        LinearLayout ll=new LinearLayout(getContext());
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(titleSpendind);
        ll.addView(AmountSpendind);
        builder.setView(ll);
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if( TextUtils.isEmpty(titleSpendind.getText()) || TextUtils.isEmpty(AmountSpendind.getText())){
                            Toast.makeText(getActivity(), "All the fields are required",
                                    Toast.LENGTH_LONG).show();
                        }else{
                            //get instance database
                            AppDatabase myDb = (new MyDatabase()).getAppDatabase();

                            //Create a new spending
                            int idBudget = myDb.tripDao().findById(getIdTrip()).getBudget();
                            Spending spending = new Spending(titleSpendind.getText().toString(),Integer.parseInt(AmountSpendind.getText().toString()),idBudget);
                            myDb.spendingDAO().insert(spending);

                            //----------------------------Update View----------------------------
                            //Update view of the amount budget
                            TextView amountBudget = (TextView)view.findViewById(R.id.textView3);
                            amountBudget.setText( myDb.budgetDao().getAmountBudget(idBudget) + "€");

                            //If there is over budget
                            if(isOverBudget(idBudget) == true){
                                TextView amountOverBudget = (TextView)view.findViewById(R.id.textView7);
                                amountOverBudget.setText("Over budget : " + (getAmountOverBudget(idBudget)));
                            }

                            //Update the progress bar
                            ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar3);
                            progressBar.setProgress(myDb.budgetDao().getAmountBudget(idBudget));

                            //Update the ListView
                            ListView maListe = (ListView) view.findViewById(R.id.list);
                            budgetModels = new ArrayList<>();
                            ArrayList<Spending> spendingsFromAbudget = (ArrayList<Spending>) myDb.spendingDAO().getAllFromidBudget(idBudget);

                            for (int i = 0; i < spendingsFromAbudget.size(); i++) {
                                Spending item = spendingsFromAbudget.get(i);
                                String strDate = new SimpleDateFormat("yyyy-MM-dd").format(item.getDateSpending());
                                budgetModels.add(new BudgetModel(item.getTitle(),strDate,String.valueOf(item.getAmount())));
                            }

                            adapter= new BudgetAdapter(budgetModels,getContext());
                            maListe.setAdapter(adapter);
                            //-------------------------------------------------------------------
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * To know if there is over budget in a budget given
     */
    public boolean isOverBudget(int idBudget){
        //get instance database
        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

        int limitBudget = myDb.budgetDao().findBudgetFromId(idBudget).getLimitBudget();
        int amountBudget =  myDb.budgetDao().getAmountBudget(idBudget);

        if( amountBudget > limitBudget ){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Calcul the amount of the over budget
     * @param idBudget
     * @return
     */
    public int getAmountOverBudget(int idBudget){
        //get instance database
        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

        int valueLimitBudget = myDb.budgetDao().findBudgetFromId(idBudget).getLimitBudget();
        int valueAmountBudget =  myDb.budgetDao().getAmountBudget(idBudget);

        return valueAmountBudget - valueLimitBudget;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //get instance database
        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_budget, container, false);

        //Id of the budget
        int idBudget = myDb.tripDao().findById(getIdTrip()).getBudget();

        //Set listener to the buttun 'New payement'
        Button buttonNewPayement = (Button)view.findViewById(R.id.buttonNewPayement);
        buttonNewPayement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpNewSpending();
            }
        });

        int valueLimitBudget = myDb.budgetDao().findBudgetFromId(idBudget).getLimitBudget();
        int valueAmountBudget =  myDb.budgetDao().getAmountBudget(idBudget);

        //Init the value of the limit budget
        TextView limitBudget = (TextView)view.findViewById(R.id.textView4);
        limitBudget.setText( "Budget limit : "+valueLimitBudget+ "€");

        //Init the value of the amount budget
        TextView amountBudget = (TextView)view.findViewById(R.id.textView3);
        amountBudget.setText( valueAmountBudget + " €");

        //If there is over budget
        if(this.isOverBudget(idBudget) == true){
            TextView amountOverBudget = (TextView)view.findViewById(R.id.textView7);
            amountOverBudget.setText("Over budget : " + (this.getAmountOverBudget(idBudget)));
        }

        //Init progresse bar
        ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar3);
        progressBar.setVisibility(ProgressBar.VISIBLE);

        //Set two time these values to stop the bug happening
        progressBar.setProgress((int)myDb.budgetDao().getAmountBudget(idBudget));
        progressBar.setMax((int)myDb.budgetDao().findBudgetFromId(idBudget).getLimitBudget());
        progressBar.setProgress((int)myDb.budgetDao().getAmountBudget(idBudget));
        progressBar.setMax((int)myDb.budgetDao().findBudgetFromId(idBudget).getLimitBudget());

        //My ListView
        ListView maListe = (ListView) view.findViewById(R.id.list);

        //Data
        ArrayList<Spending> spendingsFromAbudget = (ArrayList<Spending>) myDb.spendingDAO().getAllFromidBudget(idBudget);

        budgetModels = new ArrayList<>();
        for (int i = 0; i < spendingsFromAbudget.size(); i++) {
            Spending item = spendingsFromAbudget.get(i);

            String strDate = new SimpleDateFormat("yyyy-MM-dd").format(item.getDateSpending());

            budgetModels.add(new BudgetModel(item.getTitle(),strDate,String.valueOf(item.getAmount())));
        }

        adapter= new BudgetAdapter(budgetModels,getContext());

        maListe.setAdapter(adapter);
        maListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                BudgetModel budgetModel = budgetModels.get(position);
                Snackbar.make(view,"You add this spending the : " + budgetModel.getDate(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
            }
        });
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
