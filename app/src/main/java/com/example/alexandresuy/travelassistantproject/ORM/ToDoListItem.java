package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

import java.util.Date;

@Entity(
        foreignKeys = {
                @ForeignKey(entity = Budget.class, parentColumns = "id", childColumns = "idToDoList")
        },
        indices = {@Index("id")}
        )
public class ToDoListItem extends ListItem{
    @ColumnInfo(name = "date")
    private Date date;

    @ColumnInfo(name = "idToDoList")
    private int idToDoList;

    public ToDoListItem(String name, Date date) {
        super(name);
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getIdToDoList() {
        return idToDoList;
    }

    public void setIdToDoList(int idToDoList) {
        this.idToDoList = idToDoList;
    }
}
