package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;

@Entity(indices = {@Index("id")})
public class ToDoList extends List {
    public ToDoList(int idTrip, String name) {
        super(idTrip, name);
    }
}
