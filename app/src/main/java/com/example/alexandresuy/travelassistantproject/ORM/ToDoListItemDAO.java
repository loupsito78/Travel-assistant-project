package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ToDoListItemDAO {
    @Query("SELECT * FROM ToDoListItem")
    List<ToDoListItem> getAll();

    @Query("select * FROM TodoListItem WHERE idToDoList= :idToDoList")
    public List<ToDoListItem> getByToDoListId(int idToDoList);

    @Insert
    void insertAll(ToDoListItem... toDoListItems);

    @Insert
    void insert(ToDoListItem toDoListItem);

    @Delete
    void delete(ToDoListItem toDoListItem);

    @Query("DELETE FROM ToDoListItem")
    public void nukeTable();
}
