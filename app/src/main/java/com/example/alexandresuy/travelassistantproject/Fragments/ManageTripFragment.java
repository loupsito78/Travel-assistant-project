package com.example.alexandresuy.travelassistantproject.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.alexandresuy.travelassistantproject.ORM.AppDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.Budget;
import com.example.alexandresuy.travelassistantproject.ORM.MyDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.Spending;
import com.example.alexandresuy.travelassistantproject.ORM.Trip;
import com.example.alexandresuy.travelassistantproject.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static com.example.alexandresuy.travelassistantproject.Fragments.MainFragment.setIdTrip;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ManageTripFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ManageTripFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ManageTripFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    View view;

    public ManageTripFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ManageTripFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ManageTripFragment newInstance(String param1, String param2) {
        ManageTripFragment fragment = new ManageTripFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)  {

        view = inflater.inflate(R.layout.fragment_manage_trip, container, false);
        final View viewMainActivity = inflater.inflate(R.layout.activity_main, container, false);

        //get instance database
        final AppDatabase myDb = (new MyDatabase()).getAppDatabase();

        final List<Trip> listTrip = new ArrayList<Trip>(myDb.tripDao().getAll()) ;

        //My ListView
        final ListView myListTrip = (ListView) view.findViewById(R.id.listTrip);

        // Create a ArrayAdapter from List
        ArrayAdapter<Trip> arrayAdapter = new ArrayAdapter<Trip>(
                this.getContext(),
                android.R.layout.simple_list_item_1,
                listTrip );
        myListTrip.setAdapter(arrayAdapter);

        // Set an item click listener for ListView
        myListTrip.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Trip selectedItem = (Trip) parent.getItemAtPosition(position);
                Toast.makeText(getActivity(), "Display : " + selectedItem.getId(), Toast.LENGTH_LONG).show();
                MainFragment.setIdTrip(selectedItem.getId());

                Fragment fragment = new MainFragment();
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, fragment);
                ft.commit();
            }
        });

        myListTrip.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id){

                //Retrieve data
                Trip selectedItem = (Trip) parent.getItemAtPosition(position);
                popUpDeleteTrip(selectedItem);

                return true;
            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    public void popUpDeleteTrip(final Trip trip){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setTitle("Delete Trip");
        builder.setMessage("Are you sure you want to delete this trip ?");


        //Set a layout for the popup
        LinearLayout ll=new LinearLayout(getContext());
        ll.setOrientation(LinearLayout.VERTICAL);

        builder.setView(ll);
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        //get instance database
                        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

                        final List<Trip> listTrip = new ArrayList<Trip>(myDb.tripDao().getAll()) ;
                        List<Spending> listSpending =  myDb.spendingDAO().getAllFromidBudget(trip.getBudget());
                        Budget myBudget = myDb.budgetDao().findBudgetFromId(trip.getBudget());

                        Toast.makeText(getActivity(), "Deteling : " + trip.getName() , Toast.LENGTH_LONG).show();

                        //Delete
                        for (Iterator<Spending> j = listSpending.iterator(); j.hasNext(); ) {
                            Spending item = j.next();
                            myDb.spendingDAO().delete(item);
                        }
                        myDb.tripDao().delete(trip);
                        myDb.budgetDao().delete(myBudget);

                        //update view
                        final ListView myListTrip = (ListView) view.findViewById(R.id.listTrip);
                        ArrayAdapter<Trip> arrayAdapter = new ArrayAdapter<Trip>(
                                getContext(),
                                android.R.layout.simple_list_item_1,
                                listTrip );
                        myListTrip.setAdapter(arrayAdapter);


                        // Get More recent Trip
                        List <Trip> trip = myDb.tripDao().getAll();
                        int max=-10;
                        for (Iterator<Trip> k = trip.iterator(); k.hasNext(); ) {
                            int item = k.next().getId();
                            if(item > max ){
                                max = item;
                            }
                        }
                        setIdTrip(max);



                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
