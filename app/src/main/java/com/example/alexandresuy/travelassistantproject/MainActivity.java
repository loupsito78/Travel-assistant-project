package com.example.alexandresuy.travelassistantproject;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.alexandresuy.travelassistantproject.Fragments.BackupDocumentFragment;
import com.example.alexandresuy.travelassistantproject.Fragments.BudgetFragment;
import com.example.alexandresuy.travelassistantproject.Fragments.Calendar;
import com.example.alexandresuy.travelassistantproject.Fragments.CreateTripFragment;
import com.example.alexandresuy.travelassistantproject.Fragments.HomeFragment;
import com.example.alexandresuy.travelassistantproject.Fragments.CheckListFragment;
import com.example.alexandresuy.travelassistantproject.Fragments.MainFragment;
import com.example.alexandresuy.travelassistantproject.Fragments.ManageTripFragment;
import com.example.alexandresuy.travelassistantproject.ORM.AppDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.Country;
import com.example.alexandresuy.travelassistantproject.ORM.MyDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.Trip;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements
        NavigationView.OnNavigationItemSelectedListener,
        BudgetFragment.OnFragmentInteractionListener,
        MainFragment.OnFragmentInteractionListener,
        BackupDocumentFragment.OnFragmentInteractionListener,
        Calendar.OnFragmentInteractionListener,
        HomeFragment.OnFragmentInteractionListener,
        ManageTripFragment.OnFragmentInteractionListener,
        CreateTripFragment.OnFragmentInteractionListener,
        CheckListFragment.OnFragmentInteractionListener
{

    List<Country> listCountryFromDatabase = new ArrayList<Country>();

    private static Context mContext;

    private static Activity activity;

    public static Context getContext() {
        return mContext;
    }

    public static void setmContext(Context mContext) {
        MainActivity.mContext = mContext;
    }

    public static Activity getMainActivity() {
        return activity;
    }

    public static void setActivity(Activity activity) {
        MainActivity.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Save context in an attribut
        final Context applicationContext = getApplicationContext();
        this.setmContext(applicationContext);

        final Activity myActivity = getMainActivity();
        this.setActivity(myActivity);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        displayView(R.id.home);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        //
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        displayView(item.getItemId());
        return true;
    }


    public void displayView(int viewId) {

        Fragment fragment = null;
        String title = getString(R.string.app_name);

        //get instance database
        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

        List<Trip> listTrip = myDb.tripDao().getAll();

        if(listTrip.size() <= 0 && viewId != R.id.home){
            Toast.makeText(getContext(), "You don't have trips yet", Toast.LENGTH_LONG).show();
        } else{
            boolean hideButtonMenu = false;

            if (viewId == R.id.home) {
                fragment = new HomeFragment();
                title  = "Home";
                hideButtonMenu = true;
            } else if (viewId == R.id.nav_camera) {
                fragment = new MainFragment();
                title  = "Dashboard";
                hideButtonMenu = false;
            } else if (viewId == R.id.nav_gallery) {
                fragment = new BudgetFragment();
                title = "Budget";
                hideButtonMenu = false;
            } else if (viewId == R.id.nav_slideshow) {
                fragment = new BackupDocumentFragment();
                title = "Backup documents";
                hideButtonMenu = false;
            } else if (viewId == R.id.nav_manage) {
                // Calendar Fragment
                fragment= new Calendar();
                title = "TEST";
                hideButtonMenu = false;
            } else if (viewId == R.id.nav_share) {
                fragment = new CheckListFragment();
                title = "CheckList";
                hideButtonMenu = false;
            } else if (viewId == R.id.nav_send) {

            }

            if (fragment != null) {
                FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.content_frame, fragment);
                ft.commit();
            }
        }

        // set the toolbar title
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }
}
