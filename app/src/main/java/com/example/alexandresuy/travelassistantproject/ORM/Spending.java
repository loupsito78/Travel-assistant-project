package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;


@Entity(
        foreignKeys = {
                @ForeignKey(entity = Budget.class, parentColumns = "id", childColumns = "idBudget")
        },
        indices = {@Index("id")}
)
public class Spending {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    private int idBudget;

    @NonNull
    private String title;

    @ColumnInfo(name = "amount")
    private int amount;

    private Date dateSpending;

    public Spending(@NonNull String title, int amount,int idBudget) {
        this.title = title;
        this.amount = amount;
        this.idBudget = idBudget;
        this.dateSpending = new Date();
    }

    public int getId() {
        return id;
    }

    @NonNull
    public int getIdBudget() {
        return idBudget;
    }

    @NonNull
    public String getTitle() {
        return title;
    }

    public int getAmount() {
        return amount;
    }

    public Date getDateSpending() {
        return dateSpending;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(@NonNull String title) {
        this.title = title;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void setDateSpending(Date dateSpending) {
        this.dateSpending = dateSpending;
    }

    public void setIdBudget(@NonNull int idBudget) {
        this.idBudget = idBudget;
    }
}
