package com.example.alexandresuy.travelassistantproject.ORM;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class ListCountry {

    ArrayList<String> listCountry = new ArrayList<>(Arrays.asList(
            "France",
            "Japan",
            "United Kingdom",
            "Spain",
            "Canada",
            "Australia"
    ));

    public ArrayList<String> getListCountry() {
        return listCountry;
    }

    public void insertListCountry(){

        //get instance database
        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

        //insert all the countries
        for (Iterator<String> i = (new ListCountry()).getListCountry().iterator(); i.hasNext(); ) {
            String item = i.next();
            System.out.println(item);

            try {
                myDb.countryDao().insert(new Country(item));
            } catch(Exception e){
                //This catch block catches all the exceptions
            }
        }
    }





}
