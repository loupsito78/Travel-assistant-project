package com.example.alexandresuy.travelassistantproject.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.example.alexandresuy.travelassistantproject.CheckListItemActivity;
import com.example.alexandresuy.travelassistantproject.Converters.CheckListAdapter;
import com.example.alexandresuy.travelassistantproject.ORM.AppDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.CheckList;
import com.example.alexandresuy.travelassistantproject.ORM.List;
import com.example.alexandresuy.travelassistantproject.ORM.MyDatabase;
import com.example.alexandresuy.travelassistantproject.R;

import java.util.ArrayList;

import static com.example.alexandresuy.travelassistantproject.Fragments.MainFragment.getIdTrip;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CheckListFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CheckListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CheckListFragment extends Fragment {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    //To get the view
    View view;

    static int idCheckList;

    //List to structure the CheckLists in a budget for the ListView
    ArrayList<CheckList> checkLists;

    //Adapter for the budget
    private static CheckListAdapter adapter;

    //Parameter auto generate
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    static public int getIdCheckList() {
        return idCheckList;
    }

    public void setIdCheckList(int idCheckList) {
        this.idCheckList = idCheckList;
    }

    public CheckListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BudgetFragment.
     */
    public static CheckListFragment newInstance(String param1, String param2) {
        CheckListFragment fragment = new CheckListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    /**
     * Create a popup for a new CheckList
     */
    public void popUpNewCheckList(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setTitle("New CheckList");
        builder.setMessage("Add a new CheckList");
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // EditText in the popup
        final EditText titleList = new EditText(getContext());

        //Set type
        titleList.setInputType(InputType.TYPE_CLASS_TEXT);

        //Set placeholder
        titleList.setHint("Title");

        //Set a layout for the popup
        LinearLayout ll=new LinearLayout(getContext());
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(titleList);
        builder.setView(ll);
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if( TextUtils.isEmpty(titleList.getText())){
                            Toast.makeText(getActivity(), "All the fields are required",
                                    Toast.LENGTH_LONG).show();
                        }else{
                            //get instance database
                            AppDatabase myDb = (new MyDatabase()).getAppDatabase();

                            //Create a new CheckList
                            int idTrip = getIdTrip();

                            CheckList list = new CheckList(idTrip,titleList.getText().toString());
                            myDb.checkListDao().insert(list);

                            //----------------------------Update View----------------------------

                            //Update the ListView
                            ListView maListe = (ListView) view.findViewById(R.id.list);
                            checkLists = (ArrayList<CheckList>) myDb.checkListDao().getByTripId(idTrip);

                            adapter = new CheckListAdapter(checkLists,getContext());
                            maListe.setAdapter(adapter);
                            //-------------------------------------------------------------------
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //get instance database
        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_checklist, container, false);

        //Set listener to the button 'New List'
        Button buttonNewCheckList = (Button)view.findViewById(R.id.buttonNewCheckList);
        buttonNewCheckList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("popup","Creation popup");
                popUpNewCheckList();
            }
        });

        int idTrip = getIdTrip();

        //My ListView
        ListView maListe = (ListView) view.findViewById(R.id.list);
        checkLists = (ArrayList<CheckList>) myDb.checkListDao().getByTripId(idTrip);

        adapter= new CheckListAdapter(checkLists,getContext());

        maListe.setAdapter(adapter);
        maListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                List checkList = checkLists.get(position);
                setIdCheckList(checkList.getId());
                Log.d("checkList","id:"+checkList.getId()+",name"+checkList.getName()+"iudtrip: "+checkList.getIdTrip());
                Snackbar.make(view,"You add this spending the : " + checkList.getName(), Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                Intent intent = new Intent(getActivity(), CheckListItemActivity.class);
                intent.putExtra("checklist",checkList.getId());
                startActivity(intent);
            }
        });
        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
