package com.example.alexandresuy.travelassistantproject.Fragments;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alexandresuy.travelassistantproject.Converters.BudgetAdapter;
import com.example.alexandresuy.travelassistantproject.Converters.BudgetModel;
import com.example.alexandresuy.travelassistantproject.MainActivity;
import com.example.alexandresuy.travelassistantproject.ORM.AppDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.Budget;
import com.example.alexandresuy.travelassistantproject.ORM.MyDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.PictureInfos;
import com.example.alexandresuy.travelassistantproject.ORM.Spending;
import com.example.alexandresuy.travelassistantproject.ORM.Trip;
import com.example.alexandresuy.travelassistantproject.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.example.alexandresuy.travelassistantproject.Fragments.MainFragment.getIdTrip;
import static com.example.alexandresuy.travelassistantproject.Fragments.MainFragment.setIdTrip;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BackupDocumentFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link BackupDocumentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BackupDocumentFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    static final int REQUEST_IMAGE_CAPTURE = 1;

    //To get the view
    View view;

    private static final int MY_PERMISSIONS_REQUEST_CAMERA = 1;

    String mCurrentPhotoPath;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public BackupDocumentFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment BackupDocumentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static BackupDocumentFragment newInstance(String param1, String param2) {
        BackupDocumentFragment fragment = new BackupDocumentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_backup_document, container, false);

        //get instance database
        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

        //---------------------------------List View Backup documents---------------------------------
        final List<String> list = new ArrayList<String>() ;
        for (Iterator<PictureInfos> i = myDb.pictureInfosDAO().getAllPictureInfosFromIdTrip(MainFragment.getIdTrip()).iterator(); i.hasNext();) {
            String item = i.next().getTitlePicture();
            list.add(item);
        }

        //My ListView
        ListView myListDocuments = (ListView) view.findViewById(R.id.listDocuments);

        // Create a ArrayAdapter from List
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                this.getContext(),
                android.R.layout.simple_list_item_1,
                list );
        myListDocuments.setAdapter(arrayAdapter);

        // Set an item click listener for ListView
        myListDocuments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = (String) parent.getItemAtPosition(position);
                //Toast.makeText(getActivity(), "Display : " + selectedItem, Toast.LENGTH_LONG).show();
                popUpDisplayPicture(selectedItem);

            }
        });

        myListDocuments.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            public boolean onItemLongClick(    AdapterView<?> parent,    View v,    int position,    long id){
                String selectedItem = (String) parent.getItemAtPosition(position);
                popUpDeleteDocument(selectedItem);
                return true;
            }
        });

        //--------------------------------------------------------------------------------------------

        Button buttonNewDocument = (Button)view.findViewById(R.id.buttonNewDocument);
        buttonNewDocument.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popUpNewDocument();
            }
        });
        return view;
    }

    //To take photo with an existing app
    private void TakePictureFromExistingAppIntent(String namePicture) {
          // Permission is not granted
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            //request the permission
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);
        }
        // Permission has already been granted
        else {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the intent
            if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                // Create the File where the photo should go
                File photoFile = null;
                try {
                    photoFile = createImageFile(namePicture);
                } catch (IOException ex) {
                    // Error occurred while creating the File
                }
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    Uri photoURI = FileProvider.getUriForFile(getContext(), "com.example.android.fileprovider", photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    startActivityForResult(takePictureIntent, MY_PERMISSIONS_REQUEST_CAMERA);
                }
            }
            galleryAddPic();
        }
    }

    private File createImageFile(String namePicture) throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = namePicture + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        //get instance database
        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

        myDb.pictureInfosDAO().insert(new PictureInfos(MainFragment.getIdTrip(),image.getName()));

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    public List<File> getFilesFromDirectoryApp(){
        // Get the directory path
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        // List all the files
        File[] files = storageDir.listFiles();
        List<File> list = Arrays.asList(files);

        return list;
    }

    public void popUpNewDocument(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setTitle("New document");
        builder.setMessage("Add a new document");
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // EditText in the popup
        final EditText titleDocument = new EditText(getContext());

        //Set type
        titleDocument.setInputType(InputType.TYPE_CLASS_TEXT);

        //Set placeholder
        titleDocument.setHint("Title document");

        //Set a layout for the popup
        LinearLayout ll=new LinearLayout(getContext());
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(titleDocument);

        builder.setView(ll);
        builder.setPositiveButton("Take the picture",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        if( TextUtils.isEmpty(titleDocument.getText())){
                            Toast.makeText(getActivity(), "Please, fill the field",
                                    Toast.LENGTH_LONG).show();
                        }else{

                            String title = titleDocument.getText().toString();
                            boolean validNameDocument = true;

                            List<File> list = getFilesFromDirectoryApp();

                            for (Iterator<File> i = list.iterator(); i.hasNext();) {
                                File item = i.next();

                                if(title == item.getName()){
                                    validNameDocument = false;
                                    break;
                                }
                            }

                            if (validNameDocument == true){
                                TakePictureFromExistingAppIntent(title);

                                //get instance database
                                AppDatabase myDb = (new MyDatabase()).getAppDatabase();

                                //----------------------------Update ListView----------------------------
                                final List<String> listNamePictures = new ArrayList<String>() ;
                                for (Iterator<PictureInfos> i = myDb.pictureInfosDAO().getAllPictureInfosFromIdTrip(MainFragment.getIdTrip()).iterator(); i.hasNext();) {
                                    String item = i.next().getTitlePicture();
                                    listNamePictures.add(item);
                                }

                                //My ListView
                                ListView myListDocuments = (ListView) view.findViewById(R.id.listDocuments);

                                ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                                        getContext(),
                                        android.R.layout.simple_list_item_1,
                                        listNamePictures);

                                myListDocuments.setAdapter(arrayAdapter);
                                //-----------------------------------------------------------------------
                            }
                            else {
                                Toast.makeText(getActivity(), "This name has alrerady been taken. Please choose another one",
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public void popUpDisplayPicture(String namePicture){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        // EditText in the popup
        final ImageView documentPicture = new ImageView(getContext());

        // Get the directory path
        String storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() + "/" + namePicture;

        File imgFile = new  File(storageDir);

        if(imgFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            documentPicture.setImageBitmap(myBitmap);
        }
        else{
            Toast.makeText(getActivity(), "Error load picture : picture not found",
                    Toast.LENGTH_LONG).show();
        }

        //Set a layout for the popup
        LinearLayout ll=new LinearLayout(getContext());
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.addView(documentPicture);

        builder.setView(ll);

        AlertDialog alert = builder.create();
        alert.show();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    public void popUpDeleteDocument(final String namePicture){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCancelable(true);
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        builder.setTitle("Delete Document");
        builder.setMessage("Are you sure you want to delete this Document ?");

        //Set a layout for the popup
        LinearLayout ll=new LinearLayout(getContext());
        ll.setOrientation(LinearLayout.VERTICAL);

        builder.setView(ll);
        builder.setPositiveButton("Confirm",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        //get instance database
                        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

                        // Get the directory path
                        String storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES).toString() + "/" + namePicture;
                        File imgFile = new  File(storageDir);

                        PictureInfos myPictureInfos = myDb.pictureInfosDAO().findPictureInfosFromTitle(namePicture);

                        //Delete
                        myDb.pictureInfosDAO().delete(myPictureInfos);
                        boolean deleted = imgFile.delete();

                        Toast.makeText(getActivity(), "Deteling document : " + namePicture , Toast.LENGTH_LONG).show();

                    }
                });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
