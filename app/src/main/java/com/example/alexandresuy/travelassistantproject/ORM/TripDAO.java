package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Delete;

import java.util.List;

@Dao
public interface TripDAO {
    @Query("SELECT * FROM Trip")
    List<Trip> getAll();

    @Query("SELECT * FROM Trip WHERE id IN (:tripIds)")
    List<Trip> loadAllByIds(int[] tripIds);

    @Query("SELECT * FROM Trip WHERE name LIKE :first")
    Trip findByName(String first);

    @Query("SELECT * FROM Trip WHERE id LIKE :id")
    Trip findById(int id);

    @Insert
    void insertAll(Trip... trips);

    @Insert
    long insert(Trip trip);

    @Delete
    void delete(Trip trips);

    @Query("DELETE FROM Trip")
    public void nukeTable();
}
