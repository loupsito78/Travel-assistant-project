package com.example.alexandresuy.travelassistantproject.Converters;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.alexandresuy.travelassistantproject.R;

import java.util.ArrayList;


public class BudgetAdapter extends ArrayAdapter<BudgetModel> implements View.OnClickListener{

    private ArrayList<BudgetModel> dataSet;
    Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView title;
        TextView Date;
        TextView amountSpending;
        ImageView info;
    }

    public BudgetAdapter(ArrayList<BudgetModel> data, Context context) {
        super(context, R.layout.row_item, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        BudgetModel budgetModel =(BudgetModel)object;

        switch (v.getId())
        {
            case R.id.item_info:
                Snackbar.make(v, "Amount of your spending : " + budgetModel.getAmountSpending() , Snackbar.LENGTH_LONG)
                        .setAction("No action", null).show();
                break;
        }
    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        BudgetModel budgetModel = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.row_item, parent, false);
            viewHolder.title = (TextView) convertView.findViewById(R.id.name);
            viewHolder.Date = (TextView) convertView.findViewById(R.id.type);
            viewHolder.amountSpending = (TextView) convertView.findViewById(R.id.version_number);
            viewHolder.info = (ImageView) convertView.findViewById(R.id.item_info);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        lastPosition = position;
        viewHolder.title.setText(budgetModel.getTitle());
        viewHolder.Date.setText(budgetModel.getDate());
        viewHolder.amountSpending.setText(budgetModel.getAmountSpending() +" €");
        viewHolder.info.setOnClickListener(this);
        viewHolder.info.setTag(position);
        // Return the completed view to render on screen
        return convertView;
    }


}
