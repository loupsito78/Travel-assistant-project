package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(indices = {@Index(value ={"name"},unique=true)})
public class Country {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    private String name;

    public Country(@NonNull String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
