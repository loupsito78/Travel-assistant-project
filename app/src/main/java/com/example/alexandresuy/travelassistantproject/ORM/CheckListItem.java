package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

@Entity(
        foreignKeys = {
                @ForeignKey(entity = CheckList.class, parentColumns = "id", childColumns = "idCheckList")
        },
        indices = {@Index("id")}
        )
public class CheckListItem extends ListItem{
    @ColumnInfo(name = "number")
    private int number;

    @ColumnInfo(name = "idCheckList")
    private int idCheckList;

    public CheckListItem(int idCheckList,String name, int number) {
        super(name);
        this.number = number;
        this.idCheckList = idCheckList;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getIdCheckList() {
        return idCheckList;
    }

    public void setIdCheckList(int idCheckList) {
        this.idCheckList = idCheckList;
    }
}
