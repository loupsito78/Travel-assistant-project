package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface CheckListItemDAO {
    @Query("SELECT * FROM CheckListItem")
    List<CheckListItem> getAll();

    @Query("select * FROM CheckListItem WHERE idCheckList= :idCheckList")
    public List<CheckListItem> getByCheckListId(int idCheckList);

    @Insert
    void insertAll(CheckListItem... checkListItems);

    @Insert
    void insert(CheckListItem checkListItem);

    @Delete
    void delete(CheckListItem checkListItem);

    @Query("DELETE FROM CheckListItem")
    public void nukeTable();
}
