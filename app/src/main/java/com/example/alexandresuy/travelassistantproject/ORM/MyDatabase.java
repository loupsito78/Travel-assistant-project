package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Room;
import android.util.Log;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static com.example.alexandresuy.travelassistantproject.Fragments.MainFragment.setIdTrip;
import static com.example.alexandresuy.travelassistantproject.MainActivity.getContext;

public class MyDatabase {

    private static AppDatabase db = null;

    public AppDatabase getAppDatabase() {

        if (db == null) {
            db = Room.databaseBuilder(getContext(),
                    AppDatabase.class, "database-assistant-travel")
                    .allowMainThreadQueries()
                    .build();

            //insert all the countries
            (new ListCountry()).insertListCountry();

            //Get more recent trip
            List <Trip> trip = db.tripDao().getAll();
            int max=-10;
            for (Iterator<Trip> i = trip.iterator(); i.hasNext(); ) {
                int item = i.next().getId();
                if(item > max ){
                    max = item;
                }
            }
            setIdTrip(max);
        }
        return (db);
    }

    public static AppDatabase getDb() {
        return db;
    }
}
