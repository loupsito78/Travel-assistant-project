package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.PrimaryKey;

public abstract class List {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name= "idTrip")
    private int idTrip;

    @ColumnInfo(name = "name")
    private String name;

    public List(int idTrip, String name) {
        this.idTrip = idTrip;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTrip() {
        return idTrip;
    }

    public void setIdTrip(int idTrip) {
        this.idTrip = idTrip;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
