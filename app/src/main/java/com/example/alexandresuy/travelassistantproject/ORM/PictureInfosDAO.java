package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PictureInfosDAO {
    @Query("SELECT * FROM PictureInfos")
    List<PictureInfos> getAll();

    @Query("SELECT * FROM PictureInfos WHERE id=:id")
    PictureInfos findPictureInfosFromId(int id);

    @Query("SELECT * FROM PictureInfos WHERE titlePicture =:title")
    PictureInfos findPictureInfosFromTitle(String title);

    @Query("SELECT * FROM PictureInfos where idTrip = :idTrip")
    List<PictureInfos> getAllPictureInfosFromIdTrip(int idTrip);

    @Insert
    void insertAll(PictureInfos... pictureInfos);

    @Insert
    long insert(PictureInfos pictureInfos);

    @Delete
    void delete(PictureInfos pictureInfos);

    @Query("DELETE FROM PictureInfos")
    public void nukeTable();
}
