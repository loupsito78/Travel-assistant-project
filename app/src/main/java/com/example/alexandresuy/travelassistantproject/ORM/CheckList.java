package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;

@Entity(
        foreignKeys = {
                @ForeignKey(entity = Trip.class, parentColumns = "id", childColumns = "idTrip")
        },
        indices = {@Index("id")}
    )
public class CheckList extends List {

    public CheckList(int idTrip, String name) {
        super(idTrip, name);
    }
}
