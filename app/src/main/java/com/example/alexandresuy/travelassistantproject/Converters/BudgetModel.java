package com.example.alexandresuy.travelassistantproject.Converters;

public class BudgetModel {

    String title;
    String date;
    String amountSpending;
    String feature;

    public BudgetModel(String title, String date, String amountSpending) {
        this.title =title;
        this.date =date;
        this.amountSpending =amountSpending;

    }

    public String getTitle() {
        return title;
    }

    public String getDate() {
        return date;
    }

    public String getAmountSpending() {
        return amountSpending;
    }

}
