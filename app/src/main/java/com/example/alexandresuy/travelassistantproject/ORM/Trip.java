package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.content.ContentValues;
import android.provider.BaseColumns;

import com.example.alexandresuy.travelassistantproject.ORM.Budget;
import com.example.alexandresuy.travelassistantproject.ORM.Country;

import java.util.Date;

@Entity(
        foreignKeys = {
                @ForeignKey(entity = Country.class, parentColumns = "id", childColumns = "country"),
                @ForeignKey(entity = Budget.class, parentColumns = "id", childColumns = "budget"),
        },
        indices = {@Index("id")}
)

public class Trip {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "country")
    private int country;

    @ColumnInfo(name = "startDate")
    private Date startDate;

    @ColumnInfo(name = "endDate")
    private Date endDate;

    @ColumnInfo(name = "budget")
    private int budget;

    public Trip(String name, int country, Date startDate, Date endDate, int budget) {
        this.name = name;
        this.country = country;
        this.startDate = startDate;
        this.endDate = endDate;
        this.budget = budget;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getCountry() {
        return country;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public int getBudget() {
        return budget;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public String toString() {
        return this.getName();
    }
}
