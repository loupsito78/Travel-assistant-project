package com.example.alexandresuy.travelassistantproject.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.alexandresuy.travelassistantproject.ORM.AppDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.Budget;
import com.example.alexandresuy.travelassistantproject.ORM.MyDatabase;
import com.example.alexandresuy.travelassistantproject.ORM.Trip;
import com.example.alexandresuy.travelassistantproject.R;

import java.util.Date;
import java.util.List;

import static com.example.alexandresuy.travelassistantproject.Fragments.MainFragment.setIdTrip;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CreateTripFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CreateTripFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CreateTripFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    View view;

    public CreateTripFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CreateTripFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CreateTripFragment newInstance(String param1, String param2) {
        CreateTripFragment fragment = new CreateTripFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //get instance database
        AppDatabase myDb = (new MyDatabase()).getAppDatabase();

        List<String> COUNTRIESfromDB;
        COUNTRIESfromDB = myDb.countryDao().getAllNameCountry();

        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_create_trip, container, false);

        final TextView tripName = view.findViewById(R.id.editTextNameTrip);
        final TextView tripStartDate = view.findViewById(R.id.editTextStartDate);
        final TextView tripEndDate = view.findViewById(R.id.editTextEndDate);
        final TextView tripLimitBudget = view.findViewById(R.id.editTextLimitBudget);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(), android.R.layout.simple_dropdown_item_1line, COUNTRIESfromDB);
        final Spinner SpinnerCountry = (Spinner) view.findViewById(R.id.spinner);
        SpinnerCountry.setAdapter(adapter);

        Button buttonCreateTrip = view.findViewById(R.id.buttonCreateTrip);
        buttonCreateTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //get instance database
                AppDatabase myDb = (new MyDatabase()).getAppDatabase();

                String valueTripName = tripName.getText().toString();
                String valueTripStartDate = tripStartDate.getText().toString();
                String valueTripEndDate = tripEndDate.getText().toString();
                String valueTripLimitBudget = tripLimitBudget.getText().toString();
                String valueCountry = SpinnerCountry.getSelectedItem().toString();

                if( TextUtils.isEmpty(tripName.getText()) ||
                        TextUtils.isEmpty(tripStartDate.getText()) ||
                        TextUtils.isEmpty(tripEndDate.getText()) ||
                        TextUtils.isEmpty(tripLimitBudget.getText())
                        ){
                    Toast.makeText(getActivity(), "All the fields are required",
                            Toast.LENGTH_LONG).show();
                }else{
                    int idCountry = myDb.countryDao().getIdCountryFromName(valueCountry);
                    int limitBudget = Integer.parseInt(valueTripLimitBudget);

                    Budget myBudget = new Budget(0,limitBudget);
                    int idBudget = (int) myDb.budgetDao().insert(myBudget);
                    Trip myTrip = new Trip(valueTripName,idCountry,new Date(valueTripStartDate),new Date(valueTripEndDate), idBudget);
                    int idTripInserted = (int) myDb.tripDao().insert(myTrip);
                    setIdTrip(idTripInserted);

                    Toast.makeText(getContext(), "Trip created !", Toast.LENGTH_LONG).show();

                    Fragment fragment = new MainFragment();
                    FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.content_frame, fragment);
                    ft.commit();
                }
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
