package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface SpendingDAO {
    @Query("SELECT * FROM Spending")
    List<Spending> getAll();

    @Query("SELECT * FROM Spending where idBudget = :idBudget")
    List<Spending> getAllFromidBudget(int idBudget);

    @Query("SELECT sum(amount) FROM Spending where idBudget = :idBudget")
    public int getAmountBudget(int idBudget);

    @Insert
    void insertAll(Spending... spendings);

    @Insert
    void insert(Spending spending);

    @Delete
    void delete(Spending spending);

    @Query("DELETE FROM Spending")
    public void nukeTable();
}
