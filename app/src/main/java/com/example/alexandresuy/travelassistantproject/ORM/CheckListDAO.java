package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface CheckListDAO {
    @Query("SELECT * FROM CheckList")
    List<CheckList> getAll();

    @Query("select * FROM CheckList WHERE idTrip= :idTrip")
    List<CheckList> getByTripId(int idTrip);

    @Insert
    void insertAll(CheckList... checkLists);

    @Insert
    void insert(CheckList checkList);

    @Delete
    void delete(CheckList checkList);

    @Query("DELETE FROM CheckList")
    void nukeTable();
}
