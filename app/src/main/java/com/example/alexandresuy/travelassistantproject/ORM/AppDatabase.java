package com.example.alexandresuy.travelassistantproject.ORM;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.example.alexandresuy.travelassistantproject.Converters.Converters;

@Database(entities = {Trip.class,Country.class,Budget.class,Spending.class,PictureInfos.class, ToDoList.class, CheckList.class, ToDoListItem.class, CheckListItem.class}, version = 1, exportSchema = true)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract TripDAO tripDao();
    public abstract BudgetDAO budgetDao();
    public abstract CountryDAO countryDao();
    public abstract SpendingDAO spendingDAO();
    public abstract PictureInfosDAO pictureInfosDAO();
    public abstract ToDoListDAO toDoListDao();
    public abstract CheckListDAO checkListDao();
    public abstract ToDoListItemDAO toDoListItemDao();
    public abstract CheckListItemDAO checkListItemDao();
}
